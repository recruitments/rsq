# Recruitment task RSQ

## Application information

Application manage patients and doctors (standard CRUD operations) and makes possible to make appointments. To get information about endpoints (on locally running application) go to:

http://localhost:8080/swagger-ui.html

## Preparing application to run

* Firstly should be database setup. To do it go here and follow instructions: https://gitlab.com/recruitments/rsq_database
* Secondly you can run application using gradle or by IDE. Default listening port is set to 8080.
