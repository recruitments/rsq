package com.rsq.recruitment.businesslogic.appointments

import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.persistence.db.entities.AppointmentEntity
import com.rsq.recruitment.persistence.db.entities.DoctorEntity
import com.rsq.recruitment.persistence.db.entities.PatientEntity
import com.rsq.recruitment.persistence.db.entities.SpecializationEntity
import com.rsq.recruitment.persistence.db.services.appointments.RemoveAppointmentDBService
import com.rsq.recruitment.persistence.db.services.appointments.RetrieveAppointmentDBService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.sql.Time
import java.util.*

internal class RemoveAppointmentBusinessLogicTest {

    private val EXISTING_APPOINTMENT_ID: Long = 1

    private val NOT_EXISTING_APPOINTMENT_ID: Long = -1

    @Mock
    private lateinit var removeAppointmentDBService: RemoveAppointmentDBService

    @Mock
    private lateinit var retrieveAppointmentDBService: RetrieveAppointmentDBService

    private lateinit var removeAppointmentBusinessLogic: RemoveAppointmentBusinessLogic

    @BeforeEach
    fun before() {
        MockitoAnnotations.initMocks(this)
        removeAppointmentBusinessLogic = RemoveAppointmentBusinessLogic(retrieveAppointmentDBService, removeAppointmentDBService)
    }

    @Test
    fun removeAppointmentById() {
        `when`(retrieveAppointmentDBService.retrieveByAppointmentId(EXISTING_APPOINTMENT_ID))
                .thenReturn(
                        Optional.of(
                                AppointmentEntity(
                                        PatientEntity("Jan", "Nowak"),
                                        DoctorEntity("Wojciech", "Kowalski", SpecializationEntity("DENTIST")),
                                        Date(System.currentTimeMillis()),
                                        Time(System.currentTimeMillis()),
                                        "3 Maja")))
        removeAppointmentBusinessLogic.removeAppointmentById(EXISTING_APPOINTMENT_ID)
        verify(removeAppointmentDBService).removeAppointment(EXISTING_APPOINTMENT_ID)
    }

    @Test
    fun removeAppointmentWhenNotExists() {
        `when`(retrieveAppointmentDBService.retrieveByAppointmentId(NOT_EXISTING_APPOINTMENT_ID))
                .thenReturn(Optional.empty())
        assertThrows<ResourceNotFound> {
            removeAppointmentBusinessLogic.removeAppointmentById(NOT_EXISTING_APPOINTMENT_ID)
        }
    }
}