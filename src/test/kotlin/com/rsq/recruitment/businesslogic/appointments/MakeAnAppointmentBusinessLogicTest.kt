package com.rsq.recruitment.businesslogic.appointments

import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.communication.rest.requests.ChangeAppointmentDateRequest
import com.rsq.recruitment.communication.rest.requests.MakeAnAppointmentRequest
import com.rsq.recruitment.mappers.AppointmentEntityToAppointment
import com.rsq.recruitment.persistence.db.entities.AppointmentEntity
import com.rsq.recruitment.persistence.db.entities.DoctorEntity
import com.rsq.recruitment.persistence.db.entities.PatientEntity
import com.rsq.recruitment.persistence.db.entities.SpecializationEntity
import com.rsq.recruitment.persistence.db.services.appointments.MakeAnAppointmentDBService
import com.rsq.recruitment.persistence.db.services.appointments.RetrieveAppointmentDBService
import com.rsq.recruitment.persistence.db.services.doctors.RetrieveDoctorDBService
import com.rsq.recruitment.persistence.db.services.patients.RetrievePatientDBService
import com.rsq.recruitment.utils.toDate
import com.rsq.recruitment.utils.toTime
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.*

internal class MakeAnAppointmentBusinessLogicTest {

    private val EXISTING_APPOINTMENT_ID: Long = 3

    private val EXISTING_PATIENT_ID: Long = 1

    private val EXISTING_DOCTOR_ID: Long = 2

    private val NOT_EXISTING: Long = -1

    @Mock
    private lateinit var patientDBService: RetrievePatientDBService

    @Mock
    private lateinit var doctorDBService: RetrieveDoctorDBService

    @Mock
    private lateinit var makeAnAppointmentDBService: MakeAnAppointmentDBService

    @Mock
    private lateinit var retrieveAppointmentDBService: RetrieveAppointmentDBService

    @Mock
    private lateinit var appointmentEntityToAppointment: AppointmentEntityToAppointment

    private lateinit var makeAnAppointmentBusinessLogic: MakeAnAppointmentBusinessLogic

    @BeforeEach
    fun before() {
        MockitoAnnotations.initMocks(this)
        makeAnAppointmentBusinessLogic = MakeAnAppointmentBusinessLogic(
                doctorDBService,
                patientDBService,
                makeAnAppointmentDBService,
                retrieveAppointmentDBService,
                appointmentEntityToAppointment)
    }

    @Test
    fun makeAnAppointmentSuccess() {
        val makeAnAppointmentRequest = MakeAnAppointmentRequest(
                "2020-03-12",
                "15:00",
                "3 maja"
        )
        val patientEntity = Optional.of(PatientEntity("Wojciech", "Kowalczyk"))
        val doctorEntity = Optional.of(DoctorEntity("Jan", "Nowak", SpecializationEntity("DENTIST")))

        `when`(doctorDBService.retrieveDoctorById(EXISTING_DOCTOR_ID))
                .thenReturn(doctorEntity)
        `when`(patientDBService.retrievePatientById(EXISTING_PATIENT_ID))
                .thenReturn(patientEntity)

        makeAnAppointmentBusinessLogic.makeAnAppointment(
                EXISTING_DOCTOR_ID,
                EXISTING_PATIENT_ID,
                makeAnAppointmentRequest
        )
        verify(doctorDBService).retrieveDoctorById(EXISTING_DOCTOR_ID)
        verify(patientDBService).retrievePatientById(EXISTING_PATIENT_ID)
    }

    @Test
    fun makeAnAppointmentFailsBecauseDoctorDoesNotExists() {
        `when`(doctorDBService.retrieveDoctorById(NOT_EXISTING))
                .thenReturn(Optional.empty())
        `when`(patientDBService.retrievePatientById(EXISTING_PATIENT_ID))
                .thenReturn(Optional.of(PatientEntity("Wojciech", "Kowalczyk")))

        assertThrows<ResourceNotFound> {
            makeAnAppointmentBusinessLogic.makeAnAppointment(
                    NOT_EXISTING,
                    EXISTING_PATIENT_ID,
                    MakeAnAppointmentRequest(
                            "2020-03-12",
                            "15:00",
                            "3 maja"
                    ))
        }
    }

    @Test
    fun makeAnAppointmentFailsBecausePatientDoesNotExists() {
        `when`(doctorDBService.retrieveDoctorById(EXISTING_DOCTOR_ID))
                .thenReturn(Optional.of(DoctorEntity("Jan", "Nowak", SpecializationEntity("DENTIST"))))
        `when`(patientDBService.retrievePatientById(NOT_EXISTING))
                .thenReturn(Optional.empty())

        assertThrows<ResourceNotFound> {
            makeAnAppointmentBusinessLogic.makeAnAppointment(
                    EXISTING_DOCTOR_ID,
                    NOT_EXISTING,
                    MakeAnAppointmentRequest(
                            "2020-03-12",
                            "15:00",
                            "3 maja"
                    ))
        }
    }

    @Test
    fun changeAppointmentDateFailWhenAppointmentDoesNotExists() {
        `when`(retrieveAppointmentDBService.retrieveByAppointmentId(NOT_EXISTING))
                .thenReturn(Optional.empty())

        assertThrows<ResourceNotFound> {
            makeAnAppointmentBusinessLogic.changeAppointmentDate(
                    NOT_EXISTING,
                    ChangeAppointmentDateRequest(
                            "",
                            ""
                    ))
        }
    }

    @Test
    fun changeAppointmentDateSucceed() {
        val oldDate = toDate("2020-03-02")
        val oldTime = toTime("10:00")
        val changeAppointment = ChangeAppointmentDateRequest(
                "2020-03-03",
                "15:00"
        )
        val date = toDate(changeAppointment.date)
        val time = toTime(changeAppointment.time)
        val patientEntity = Optional.of(PatientEntity("Wojciech", "Kowalczyk"))
        val doctorEntity = Optional.of(DoctorEntity("Jan", "Nowak", SpecializationEntity("DENTIST")))
        val appointment = AppointmentEntity(
                patientEntity.get(),
                doctorEntity.get(),
                oldDate,
                oldTime,
                ""
        )

        `when`(retrieveAppointmentDBService.retrieveByAppointmentId(EXISTING_APPOINTMENT_ID))
                .thenReturn(Optional.of(appointment))

        makeAnAppointmentBusinessLogic.changeAppointmentDate(
                EXISTING_APPOINTMENT_ID,
                changeAppointment)

        assertEquals(date, appointment.date)
        assertEquals(time, appointment.time)
        verify(makeAnAppointmentDBService).saveAppointment(appointment)

    }

}