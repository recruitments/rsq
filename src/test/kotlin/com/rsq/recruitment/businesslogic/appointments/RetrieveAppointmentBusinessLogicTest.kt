package com.rsq.recruitment.businesslogic.appointments

import com.rsq.recruitment.mappers.AppointmentEntityToAppointment
import com.rsq.recruitment.persistence.db.entities.AppointmentEntity
import com.rsq.recruitment.persistence.db.entities.DoctorEntity
import com.rsq.recruitment.persistence.db.entities.PatientEntity
import com.rsq.recruitment.persistence.db.entities.SpecializationEntity
import com.rsq.recruitment.persistence.db.services.appointments.RetrieveAppointmentDBService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.sql.Time
import java.util.*

internal class RetrieveAppointmentBusinessLogicTest {

    @Mock
    private lateinit var retrieveAppointmentDBService: RetrieveAppointmentDBService

    @Mock
    private lateinit var appointmentEntityToAppointment: AppointmentEntityToAppointment

    private lateinit var retrieveAppointmentBusinessLogic: RetrieveAppointmentBusinessLogic

    @BeforeEach
    fun before() {
        MockitoAnnotations.initMocks(this)
        retrieveAppointmentBusinessLogic = RetrieveAppointmentBusinessLogic(retrieveAppointmentDBService, appointmentEntityToAppointment)
    }

    @Test
    fun retrieveAllAppointments() {
        val appointmentEntity = AppointmentEntity(
                PatientEntity("Jan", "Nowak"),
                DoctorEntity("Wojciech", "Stankiewicz", SpecializationEntity("DENTIST")),
                Date(System.currentTimeMillis()),
                Time(System.currentTimeMillis()),
                "3 maja"
        )
        `when`(retrieveAppointmentDBService.retrieveAll())
                .thenReturn(listOf(appointmentEntity))

        retrieveAppointmentBusinessLogic.retrieveAllAppointments()
        verify(appointmentEntityToAppointment).convert(appointmentEntity)
    }

    @Test
    fun retrieveAppointmentsForPatient() {
        val patientId: Long = 1
        val appointmentEntity = AppointmentEntity(
                PatientEntity("Jan", "Nowak"),
                DoctorEntity("Wojciech", "Stankiewicz", SpecializationEntity("DENTIST")),
                Date(System.currentTimeMillis()),
                Time(System.currentTimeMillis()),
                "3 maja"
        )
        `when`(retrieveAppointmentDBService.retrieveByPatientId(patientId))
                .thenReturn(listOf(appointmentEntity))

        retrieveAppointmentBusinessLogic.retrieveAppointmentsForPatient(patientId)
        verify(appointmentEntityToAppointment).convert(appointmentEntity)
    }
}