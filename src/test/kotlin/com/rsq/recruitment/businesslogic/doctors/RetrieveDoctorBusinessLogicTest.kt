package com.rsq.recruitment.businesslogic.doctors

import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.mappers.DoctorEntityToDoctor
import com.rsq.recruitment.persistence.db.entities.DoctorEntity
import com.rsq.recruitment.persistence.db.entities.SpecializationEntity
import com.rsq.recruitment.persistence.db.services.doctors.RetrieveDoctorDBService
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.*

internal class RetrieveDoctorBusinessLogicTest {

    private val NON_EXISTING_DOCTOR_ID: Long = -1

    private val EXISTING_DOCTOR_ID: Long = 1

    @Mock
    private lateinit var retrieveDoctorDBService: RetrieveDoctorDBService

    @Mock
    private lateinit var doctorEntityToDoctor: DoctorEntityToDoctor

    private lateinit var retrieveDoctorBusinessLogic: RetrieveDoctorBusinessLogic

    private val existingDoctor = DoctorEntity("Jan", "Nowak", SpecializationEntity("DENTIST"))

    @BeforeEach
    fun before() {
        MockitoAnnotations.initMocks(this)
        retrieveDoctorBusinessLogic = RetrieveDoctorBusinessLogic(retrieveDoctorDBService, doctorEntityToDoctor)
    }

    @Test
    fun retrieveDoctorByIdWhenNotExistsThrowException() {
        `when`(retrieveDoctorDBService.retrieveDoctorById(NON_EXISTING_DOCTOR_ID))
                .thenReturn(Optional.empty())
        assertThrows(ResourceNotFound::class.java) {
            retrieveDoctorBusinessLogic.retrieveDoctorById(NON_EXISTING_DOCTOR_ID)
        }
    }

    @Test
    fun retrieveDoctorByIdWhenExists() {
        `when`(retrieveDoctorDBService.retrieveDoctorById(EXISTING_DOCTOR_ID))
                .thenReturn(Optional.of(existingDoctor))
        retrieveDoctorBusinessLogic.retrieveDoctorById(EXISTING_DOCTOR_ID)
        verify(doctorEntityToDoctor).convert(existingDoctor)
    }

    @Test
    fun retrieveAllDoctorsWhenNotExists() {
        `when`(retrieveDoctorDBService.retrieveAllDoctors())
                .thenReturn(Collections.emptyList())

        val allDoctors = retrieveDoctorBusinessLogic.retrieveAllDoctors()
        assertThat(allDoctors).isEmpty()
    }

    @Test
    fun retrieveAllDoctorsWhenExists() {
        val existingDoctor2 = DoctorEntity("Wojciech", "Tymon", SpecializationEntity("DENTIST"))
        `when`(retrieveDoctorDBService.retrieveAllDoctors())
                .thenReturn(Arrays.asList(existingDoctor, existingDoctor2))

        val allDoctors = retrieveDoctorBusinessLogic.retrieveAllDoctors()
        verify(doctorEntityToDoctor).convert(existingDoctor)
        verify(doctorEntityToDoctor).convert(existingDoctor2)
        assertThat(allDoctors).hasSize(2)
    }

}