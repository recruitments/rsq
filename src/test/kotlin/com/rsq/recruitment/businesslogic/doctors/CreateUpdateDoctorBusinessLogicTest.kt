package com.rsq.recruitment.businesslogic.doctors

import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.communication.rest.requests.CreateDoctorRequest
import com.rsq.recruitment.communication.rest.requests.UpdateDoctorRequest
import com.rsq.recruitment.mappers.DoctorEntityToDoctor
import com.rsq.recruitment.persistence.db.entities.DoctorEntity
import com.rsq.recruitment.persistence.db.entities.SpecializationEntity
import com.rsq.recruitment.persistence.db.repositories.SpecializationRepository
import com.rsq.recruitment.persistence.db.services.doctors.CreateUpdateDoctorDBService
import com.rsq.recruitment.persistence.db.services.doctors.RetrieveDoctorDBService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.util.*

internal class CreateUpdateDoctorBusinessLogicTest {

    private val NOT_EXISTING_ID: Int = -1

    @Mock
    private lateinit var retrieveDoctorDBService: RetrieveDoctorDBService

    @Mock
    private lateinit var createUpdateDoctorDBService: CreateUpdateDoctorDBService

    @Mock
    private lateinit var specializationRepository: SpecializationRepository

    @Mock
    private lateinit var doctorEntityToDoctor: DoctorEntityToDoctor

    private lateinit var createUpdateDoctorBusinessLogic: CreateUpdateDoctorBusinessLogic

    @BeforeEach
    fun before() {
        MockitoAnnotations.initMocks(this)
        createUpdateDoctorBusinessLogic = CreateUpdateDoctorBusinessLogic(
                retrieveDoctorDBService,
                createUpdateDoctorDBService,
                specializationRepository,
                doctorEntityToDoctor)
    }

    @Test
    fun addDoctor() {
    }

    @Test
    fun addDoctorThrowsResourceNotFoundWhenSpecializationDoesNotExists() {
        `when`(specializationRepository.findById(-1))
                .thenReturn(Optional.empty())

        assertThrows<ResourceNotFound> {
            createUpdateDoctorBusinessLogic.addDoctor(CreateDoctorRequest("", "", -1))
        }
    }

    @Test
    fun updateDoctor() {
        val existingDoctor = DoctorEntity("Wojciech", "Kowalczyk", SpecializationEntity("DENTIST", 1), 1)
        val updateDoctorRequest = UpdateDoctorRequest("Jan", "Nowak", 3)
        val specialization = SpecializationEntity("PSYCHOLOGIST", 3)

        `when`(retrieveDoctorDBService.retrieveDoctorById(existingDoctor.doctorId!!))
                .thenReturn(Optional.of(existingDoctor))
        `when`(specializationRepository.findById(updateDoctorRequest.specializationId!!))
                .thenReturn(Optional.of(specialization))

        createUpdateDoctorBusinessLogic.updateDoctor(existingDoctor.doctorId!!, updateDoctorRequest)

        Mockito.verify(createUpdateDoctorDBService).createUpdateDoctor(existingDoctor)
        assertEquals(updateDoctorRequest.firstName, existingDoctor.firstName)
        assertEquals(updateDoctorRequest.lastName, existingDoctor.lastName)
        assertEquals(updateDoctorRequest.specializationId!!, existingDoctor.specialization.specializationId!!)
    }

    @Test
    fun updateDoctorThrowsResourceNotFoundWhenSpecializationDoesNotExists() {
        `when`(specializationRepository.findById(-1))
                .thenReturn(Optional.empty())

        assertThrows<ResourceNotFound> {
            createUpdateDoctorBusinessLogic.updateDoctor(1, UpdateDoctorRequest(
                    "", "", -1))
        }
    }

    @Test
    fun updateDoctorThrowsResourceNotFoundWhenDoctorDoesNotExists() {
        `when`(retrieveDoctorDBService.retrieveDoctorById(-1))
                .thenReturn(Optional.empty())

        assertThrows<ResourceNotFound> {
            createUpdateDoctorBusinessLogic.updateDoctor(1, UpdateDoctorRequest(
                    "", "", -1))
        }
    }

}