package com.rsq.recruitment.businesslogic.doctors

import com.rsq.recruitment.businesslogic.exceptions.DoctorHasExistingAppointments
import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.persistence.db.entities.DoctorEntity
import com.rsq.recruitment.persistence.db.entities.SpecializationEntity
import com.rsq.recruitment.persistence.db.services.appointments.RetrieveAppointmentDBService
import com.rsq.recruitment.persistence.db.services.doctors.RemoveDoctorDBService
import com.rsq.recruitment.persistence.db.services.doctors.RetrieveDoctorDBService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.*

internal class RemoveDoctorBusinessLogicTest {

    private val NON_EXISTING_DOCTOR_ID: Long = -1

    private val EXISTING_DOCTOR_ID: Long = 1

    @Mock
    private lateinit var removeDoctorDBService: RemoveDoctorDBService

    @Mock
    private lateinit var retrieveDoctorDBService: RetrieveDoctorDBService

    @Mock
    private lateinit var retrieveAppointmentDBService: RetrieveAppointmentDBService

    private lateinit var removeDoctorBusinessLogic: RemoveDoctorBusinessLogic

    @BeforeEach
    fun before() {
        MockitoAnnotations.initMocks(this)
        removeDoctorBusinessLogic = RemoveDoctorBusinessLogic(retrieveDoctorDBService, retrieveAppointmentDBService, removeDoctorDBService)
    }

    @Test
    fun deleteWhenNotExistsThrowsResourceNotFound() {
        `when`(retrieveDoctorDBService.retrieveDoctorById(NON_EXISTING_DOCTOR_ID))
                .thenReturn(Optional.empty())

        assertThrows<ResourceNotFound> {
            removeDoctorBusinessLogic.removeDoctorById(NON_EXISTING_DOCTOR_ID)
        }
    }

    @Test
    fun deleteWhenDoctorHasAppointmentsThenThrowException() {
        `when`(retrieveDoctorDBService.retrieveDoctorById(EXISTING_DOCTOR_ID))
                .thenReturn(Optional.of(DoctorEntity("Jan", "Nowak", SpecializationEntity("DENTIST"))))
        `when`(retrieveAppointmentDBService.hasDoctorAnyAppointments(EXISTING_DOCTOR_ID))
                .thenReturn(true)

        assertThrows<DoctorHasExistingAppointments> {
            removeDoctorBusinessLogic.removeDoctorById(EXISTING_DOCTOR_ID)
        }
    }

    @Test
    fun deleteWhenExistsDeletesDoctor() {
        `when`(retrieveDoctorDBService.retrieveDoctorById(EXISTING_DOCTOR_ID))
                .thenReturn(Optional.of(DoctorEntity("Jan", "Nowak", SpecializationEntity("DENTIST"))))
        `when`(retrieveAppointmentDBService.hasDoctorAnyAppointments(EXISTING_DOCTOR_ID))
                .thenReturn(false)

        removeDoctorBusinessLogic.removeDoctorById(EXISTING_DOCTOR_ID)

        verify(removeDoctorDBService).removeDoctor(EXISTING_DOCTOR_ID)
    }

}