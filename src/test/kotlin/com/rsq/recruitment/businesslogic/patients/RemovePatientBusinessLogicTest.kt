package com.rsq.recruitment.businesslogic.patients

import com.rsq.recruitment.businesslogic.exceptions.PatientHasExistingAppointments
import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.persistence.db.entities.PatientEntity
import com.rsq.recruitment.persistence.db.services.appointments.RetrieveAppointmentDBService
import com.rsq.recruitment.persistence.db.services.patients.RemovePatientDBService
import com.rsq.recruitment.persistence.db.services.patients.RetrievePatientDBService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.*

internal class RemovePatientBusinessLogicTest {

    private val NON_EXISTING_PATIENT_ID: Long = -1

    private val EXISTING_PATIENT_ID: Long = 1

    @Mock
    private lateinit var removePatientDBService: RemovePatientDBService

    @Mock
    private lateinit var retrievePatientDBService: RetrievePatientDBService

    @Mock
    private lateinit var retrieveAppointmentDBService: RetrieveAppointmentDBService

    private lateinit var removePatientBusinessLogic: RemovePatientBusinessLogic

    @BeforeEach
    fun before() {
        MockitoAnnotations.initMocks(this)
        removePatientBusinessLogic = RemovePatientBusinessLogic(retrievePatientDBService, retrieveAppointmentDBService, removePatientDBService)
    }

    @Test
    fun deleteWhenNotExistsThrowsResourceNotFound() {
        `when`(retrievePatientDBService.retrievePatientById(NON_EXISTING_PATIENT_ID))
                .thenReturn(Optional.empty())

        assertThrows<ResourceNotFound> {
            removePatientBusinessLogic.removePatientById(NON_EXISTING_PATIENT_ID)
        }
    }

    @Test
    fun deleteWhenPatientHasAppointmentsThenThrowsException() {
        `when`(retrievePatientDBService.retrievePatientById(EXISTING_PATIENT_ID))
                .thenReturn(Optional.of(PatientEntity("Jan", "Nowak")))
        `when`(retrieveAppointmentDBService.hasPatientAnyAppointments(EXISTING_PATIENT_ID))
                .thenReturn(true)

        assertThrows<PatientHasExistingAppointments> {
            removePatientBusinessLogic.removePatientById(EXISTING_PATIENT_ID)
        }
    }

    @Test
    fun deleteWhenExistsDeletesPatient() {
        `when`(retrievePatientDBService.retrievePatientById(EXISTING_PATIENT_ID))
                .thenReturn(Optional.of(PatientEntity("Jan", "Nowak")))
        `when`(retrieveAppointmentDBService.hasPatientAnyAppointments(EXISTING_PATIENT_ID))
                .thenReturn(false)

        removePatientBusinessLogic.removePatientById(EXISTING_PATIENT_ID)

        verify(removePatientDBService).removePatient(EXISTING_PATIENT_ID)
    }

}