package com.rsq.recruitment.businesslogic.patients

import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.communication.rest.requests.CreatePatientRequest
import com.rsq.recruitment.communication.rest.requests.UpdatePatientRequest
import com.rsq.recruitment.mappers.PatientEntityToPatient
import com.rsq.recruitment.persistence.db.entities.PatientEntity
import com.rsq.recruitment.persistence.db.services.patients.CreateUpdatePatientDBService
import com.rsq.recruitment.persistence.db.services.patients.RetrievePatientDBService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.*

internal class CreateUpdatePatientBusinessLogicTest {

    @Mock
    private lateinit var retrievePatientDBService: RetrievePatientDBService

    @Mock
    private lateinit var createUpdatePatientDBService: CreateUpdatePatientDBService

    @Mock
    private lateinit var patientEntityToPatient: PatientEntityToPatient

    private lateinit var createUpdatePatientBusinessLogic: CreateUpdatePatientBusinessLogic

    @BeforeEach
    fun before() {
        MockitoAnnotations.initMocks(this)
        createUpdatePatientBusinessLogic = CreateUpdatePatientBusinessLogic(
                retrievePatientDBService,
                createUpdatePatientDBService,
                patientEntityToPatient)
    }

    @Test
    fun addPatient() {
        val createPatientRequest = CreatePatientRequest("Jan", "Nowak", "1 maja")
        val patientEntity = PatientEntity(
                createPatientRequest.firstName!!,
                createPatientRequest.lastName!!,
                createPatientRequest.address)
        `when`(createUpdatePatientDBService.createUpdatePatient(com.rsq.recruitment.any()))
                .thenReturn(patientEntity)

        createUpdatePatientBusinessLogic.addPatient(createPatientRequest)

        verify(patientEntityToPatient).convert(patientEntity)
    }

    @Test
    fun updatePatientSucceed() {
        val existingPatient = PatientEntity("Wojciech", "Kowalczyk", "1 maja", 1)
        val updatePatientRequest = UpdatePatientRequest("Jan", "Nowak", "1 maja")

        `when`(retrievePatientDBService.retrievePatientById(existingPatient.patientId!!))
                .thenReturn(Optional.of(existingPatient))

        createUpdatePatientBusinessLogic.updatePatient(existingPatient.patientId!!, updatePatientRequest)

        verify(createUpdatePatientDBService).createUpdatePatient(existingPatient)
        assertEquals(updatePatientRequest.firstName, existingPatient.firstName)
        assertEquals(updatePatientRequest.lastName, existingPatient.lastName)
        assertEquals(updatePatientRequest.address, existingPatient.address)
    }

    @Test
    fun updatePatientThrowsResourceNotFoundWhenPatientDoesNotExists() {
        `when`(retrievePatientDBService.retrievePatientById(-1))
                .thenReturn(Optional.empty())

        assertThrows<ResourceNotFound> {
            createUpdatePatientBusinessLogic.updatePatient(
                    -1,
                    UpdatePatientRequest())
        }
    }

}