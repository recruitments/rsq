package com.rsq.recruitment.businesslogic.patients

import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.mappers.PatientEntityToPatient
import com.rsq.recruitment.persistence.db.entities.PatientEntity
import com.rsq.recruitment.persistence.db.services.patients.RetrievePatientDBService
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.*

internal class RetrievePatientBusinessLogicTest {

    private val NON_EXISTING_PATIENT_ID: Long = -1

    private val EXISTING_PATIENT_ID: Long = 1

    @Mock
    private lateinit var retrievePatientDBService: RetrievePatientDBService

    @Mock
    private lateinit var patientEntityToPatient: PatientEntityToPatient

    private lateinit var retrievePatientBusinessLogic: RetrievePatientBusinessLogic

    private var existingPatient = PatientEntity("Jan", "Nowak")

    @BeforeEach
    fun before() {
        MockitoAnnotations.initMocks(this)
        retrievePatientBusinessLogic = RetrievePatientBusinessLogic(retrievePatientDBService, patientEntityToPatient)
    }

    @Test
    fun retrievePatientByIdWhenNotExistsThrowException() {
        Mockito.`when`(retrievePatientDBService.retrievePatientById(NON_EXISTING_PATIENT_ID))
                .thenReturn(Optional.empty())
        assertThrows(ResourceNotFound::class.java) {
            retrievePatientBusinessLogic.retrievePatientById(NON_EXISTING_PATIENT_ID)
        }
    }

    @Test
    fun retrievePatientByIdWhenExists() {
        Mockito.`when`(retrievePatientDBService.retrievePatientById(EXISTING_PATIENT_ID))
                .thenReturn(Optional.of(existingPatient))
        retrievePatientBusinessLogic.retrievePatientById(EXISTING_PATIENT_ID)
        Mockito.verify(patientEntityToPatient).convert(existingPatient)
    }

    @Test
    fun retrieveAllPatientsWhenNotExists() {
        Mockito.`when`(retrievePatientDBService.retrieveAllPatients())
                .thenReturn(Collections.emptyList())

        val allPatients = retrievePatientBusinessLogic.retrieveAllPatients()
        Assertions.assertThat(allPatients).isEmpty()
    }

    @Test
    fun retrieveAllPatientsWhenExists() {
        val existingPatient2 = PatientEntity("Wojciech", "Tymon")
        Mockito.`when`(retrievePatientDBService.retrieveAllPatients())
                .thenReturn(listOf(existingPatient, existingPatient2))

        val allPatients = retrievePatientBusinessLogic.retrieveAllPatients()
        Mockito.verify(patientEntityToPatient).convert(existingPatient)
        Mockito.verify(patientEntityToPatient).convert(existingPatient2)
        Assertions.assertThat(allPatients).hasSize(2)
    }
}