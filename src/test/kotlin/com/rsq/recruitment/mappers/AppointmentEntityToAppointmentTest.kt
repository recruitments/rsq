package com.rsq.recruitment.mappers

import com.rsq.recruitment.DATE_OF_26_03_2020
import com.rsq.recruitment.TIME_OF_14_00
import com.rsq.recruitment.any
import com.rsq.recruitment.businesslogic.doctors.beans.Doctor
import com.rsq.recruitment.businesslogic.patients.beans.Patient
import com.rsq.recruitment.persistence.db.entities.AppointmentEntity
import com.rsq.recruitment.persistence.db.entities.DoctorEntity
import com.rsq.recruitment.persistence.db.entities.PatientEntity
import com.rsq.recruitment.persistence.db.entities.SpecializationEntity
import com.rsq.recruitment.utils.toTime
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.sql.Time
import java.util.*

internal class AppointmentEntityToAppointmentTest {

    @Mock
    private lateinit var doctorEntityToDoctor: DoctorEntityToDoctor

    @Mock
    private lateinit var patientEntityToPatient: PatientEntityToPatient

    private lateinit var appointmentEntityToAppointment: AppointmentEntityToAppointment

    @BeforeEach
    fun before() {
        MockitoAnnotations.initMocks(this)
        appointmentEntityToAppointment = AppointmentEntityToAppointment(
                doctorEntityToDoctor,
                patientEntityToPatient)
    }

    @Test
    fun convert() {
        val date = Date(DATE_OF_26_03_2020)
        val time = Time(TIME_OF_14_00)
        val patientEntity = PatientEntity("Jan", "Nowak")
        val doctorEntity = DoctorEntity("Wojciech", "Kowalczyk", SpecializationEntity("DENTIST"))
        val appointmentEntity = AppointmentEntity(
                patientEntity,
                doctorEntity,
                date,
                time,
                "1 maja",
                33)
        Mockito.`when`(doctorEntityToDoctor.convert(any()))
                .thenReturn(Doctor(1, "Wojciech", "Kowalczyk", "DENTIST"))
        Mockito.`when`(patientEntityToPatient.convert(any()))
                .thenReturn(Patient(1, "Jan", "Nowak"))

        val appointment = appointmentEntityToAppointment.convert(appointmentEntity)

        verify(patientEntityToPatient).convert(patientEntity)
        verify(doctorEntityToDoctor).convert(doctorEntity)
        assertEquals("2020-03-26", appointment.date)
        assertEquals("14:00", appointment.hour)
        assertEquals("1 maja", appointment.address)
        assertEquals(33, appointment.appointmentId)
    }

}