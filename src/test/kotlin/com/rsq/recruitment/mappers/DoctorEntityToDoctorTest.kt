package com.rsq.recruitment.mappers

import com.rsq.recruitment.persistence.db.entities.DoctorEntity
import com.rsq.recruitment.persistence.db.entities.SpecializationEntity
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

internal class DoctorEntityToDoctorTest {

    @Mock
    private lateinit var translationService: TranslationService

    private lateinit var doctorEntityToDoctor: DoctorEntityToDoctor

    @BeforeEach
    fun before() {
        MockitoAnnotations.initMocks(this)
        doctorEntityToDoctor = DoctorEntityToDoctor(translationService)
    }

    @Test
    fun convert() {
        val doctorEntity = DoctorEntity("Jan", "Nowak", SpecializationEntity("DENTIST", 1), 1)
        Mockito.`when`(translationService.translate("DENTIST", "Dentist"))
                .thenReturn("Dentysta")

        val doctor = doctorEntityToDoctor.convert(doctorEntity)

        assertEquals("Jan", doctor.firstName)
        assertEquals("Nowak", doctor.lastName)
        assertEquals(1, doctor.doctorId)
        assertEquals("Dentysta", doctor.specialization)
    }
}