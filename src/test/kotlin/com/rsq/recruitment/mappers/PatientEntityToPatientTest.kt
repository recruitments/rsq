package com.rsq.recruitment.mappers

import com.rsq.recruitment.persistence.db.entities.PatientEntity
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.mockito.MockitoAnnotations

internal class PatientEntityToPatientTest {

    private val patientEntityToPatient = PatientEntityToPatient()

    @Test
    fun convert() {
        val patientEntity = PatientEntity("Jan", "Nowak", null, 1)

        val patient = patientEntityToPatient.convert(patientEntity)

        assertEquals("Jan", patient.firstName)
        assertEquals("Nowak", patient.lastName)
        assertNull(patient.address)
        assertEquals(1, patient.patientId)
    }

    @Test
    fun convertWhenAddressNotNull() {
        val patientEntity = PatientEntity("Jan", "Nowak", "2 maja", 1)

        val patient = patientEntityToPatient.convert(patientEntity)

        assertEquals("Jan", patient.firstName)
        assertEquals("Nowak", patient.lastName)
        assertEquals("2 maja", patient.address)
        assertEquals(1, patient.patientId)
    }

}