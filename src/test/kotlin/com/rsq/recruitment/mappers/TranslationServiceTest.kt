package com.rsq.recruitment.mappers

import com.rsq.recruitment.persistence.db.WithDatabaseTestData
import com.rsq.recruitment.persistence.db.repositories.TranslationRepository
import com.rsq.recruitment.persistence.db.services.appointments.MakeAnAppointmentDBService
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.i18n.LocaleContextHolder
import java.util.*

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class TranslationServiceTest: WithDatabaseTestData() {

    @Autowired
    private lateinit var translationRepository: TranslationRepository

    private lateinit var translationService: TranslationService

    @BeforeAll
    fun before() {
        translationService = TranslationService(translationRepository)
    }

    @Test
    fun translate() {
        LocaleContextHolder.setLocale(Locale.ENGLISH)
        val translated = translationService.translate("DENTIST", "dentysta")
        assertEquals("Dentist", translated)
    }

    @Test
    fun translatePl() {
        LocaleContextHolder.setLocale(Locale.forLanguageTag("pl"))
        val translated = translationService.translate("DENTIST", "dentysta")
        assertEquals("Dentysta", translated)
    }

    @Test
    fun testTranslateFallback() {
        val translated = translationService.translate("NON_EXISTING_ENTRY", "fallback")
        assertEquals("fallback", translated)
    }
}