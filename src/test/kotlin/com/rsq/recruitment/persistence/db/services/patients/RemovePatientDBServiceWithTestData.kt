package com.rsq.recruitment.persistence.db.services.patients

import com.rsq.recruitment.persistence.db.WithDatabaseTestData
import com.rsq.recruitment.persistence.db.repositories.AppointmentRepository
import com.rsq.recruitment.persistence.db.repositories.PatientRepository
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RemovePatientDBServiceWithTestData : WithDatabaseTestData() {

    @Autowired
    private lateinit var patientRepository: PatientRepository

    @Autowired
    private lateinit var appointmentRepository: AppointmentRepository

    private lateinit var removePatientDBService: RemovePatientDBService

    @BeforeAll
    fun before() {
        removePatientDBService = RemovePatientDBService(patientRepository)
    }

    @Test
    fun remove() {
        appointmentRepository.deleteAll()

        removePatientDBService.removePatient(firstPatient.patientId)

        assertTrue(patientRepository.findById(firstPatient.patientId!!).isEmpty)
    }

}