package com.rsq.recruitment.persistence.db.services.patients

import com.rsq.recruitment.persistence.db.WithDatabaseTestData
import com.rsq.recruitment.persistence.db.entities.PatientEntity
import com.rsq.recruitment.persistence.db.repositories.PatientRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateUpdatePatientDBServiceWithTestData : WithDatabaseTestData() {

    @Autowired
    private lateinit var patientRepository: PatientRepository

    private lateinit var createUpdatePatientDBService: CreateUpdatePatientDBService

    @BeforeAll
    fun before() {
        createUpdatePatientDBService = CreateUpdatePatientDBService(patientRepository)
    }

    @Test
    fun createNewPatient() {
        var newPatient = PatientEntity("Hello", "World")
        var createdPatient = createUpdatePatientDBService.createUpdatePatient(newPatient)

        assertEquals(newPatient.firstName, createdPatient.firstName)
        assertEquals(newPatient.lastName, createdPatient.lastName)
        assertNotNull(createdPatient.patientId)
    }

    @Test
    fun updateExistingPatient() {
        firstPatient.firstName = "Nowe_imie"
        createUpdatePatientDBService.createUpdatePatient(firstPatient)

        var updatedPatient = patientRepository.findById(firstPatient.patientId!!)

        assertEquals(updatedPatient.get().firstName, "Nowe_imie")
    }

}