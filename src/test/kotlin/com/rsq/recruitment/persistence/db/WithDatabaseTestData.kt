package com.rsq.recruitment.persistence.db

import com.rsq.recruitment.persistence.db.entities.*
import com.rsq.recruitment.persistence.db.repositories.*
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.sql.Time
import java.util.*

@SpringBootTest
class WithDatabaseTestData {

    protected lateinit var dentistSpecialization: SpecializationEntity

    protected lateinit var physiotherapistSpecialization: SpecializationEntity

    protected lateinit var dentistDoctor: DoctorEntity

    protected lateinit var physiotherapistDoctor: DoctorEntity

    protected lateinit var firstPatient: PatientEntity

    protected lateinit var secondPatient: PatientEntity

    protected lateinit var appointmentForFirstPatientAndDentist: AppointmentEntity

    @Autowired
    private lateinit var doctorRepository: DoctorRepository

    @Autowired
    private lateinit var specializationRepository: SpecializationRepository

    @Autowired
    private lateinit var patientRepository: PatientRepository

    @Autowired
    private lateinit var appointmentRepository: AppointmentRepository

    @Autowired
    private lateinit var translationRepository: TranslationRepository

    @BeforeEach
    fun prepareData() {
        appointmentRepository.deleteAll()
        doctorRepository.deleteAll()
        patientRepository.deleteAll()
        specializationRepository.deleteAll()

        translationRepository.save(TranslationEntity("Dentysta", "Dentist", "DENTIST"))
        dentistSpecialization = specializationRepository.save(SpecializationEntity("DENTIST"))
        physiotherapistSpecialization = specializationRepository.save(SpecializationEntity("PHYSIOTHERAPIST"))
        dentistDoctor = doctorRepository.save(DoctorEntity("Jan", "Kowalski", dentistSpecialization))
        physiotherapistDoctor = doctorRepository.save(DoctorEntity("Maciej", "Nowak", physiotherapistSpecialization))
        firstPatient = patientRepository.save(PatientEntity("Marzena", "Wiśniewska"))
        secondPatient = patientRepository.save(PatientEntity("Wojciech", "Brzęczyszczykiewicz"))
        appointmentForFirstPatientAndDentist = appointmentRepository.save(AppointmentEntity(
                firstPatient,
                dentistDoctor,
                Date(System.currentTimeMillis()),
                Time(System.currentTimeMillis()),
                "3 Maja"))
    }

    @Test
    fun dataCreatedSuccessfully() {
        assertNotNull(dentistDoctor)
        assertNotNull(dentistSpecialization)
        assertNotNull(physiotherapistSpecialization)
        assertNotNull(dentistDoctor)
        assertNotNull(physiotherapistDoctor)
        assertNotNull(firstPatient)
        assertNotNull(secondPatient)
        assertNotNull(appointmentForFirstPatientAndDentist)
    }

}