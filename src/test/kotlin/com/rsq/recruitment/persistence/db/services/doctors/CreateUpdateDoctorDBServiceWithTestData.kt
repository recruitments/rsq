package com.rsq.recruitment.persistence.db.services.doctors

import com.rsq.recruitment.persistence.db.WithDatabaseTestData
import com.rsq.recruitment.persistence.db.entities.DoctorEntity
import com.rsq.recruitment.persistence.db.repositories.DoctorRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateUpdateDoctorDBServiceWithTestData : WithDatabaseTestData() {

    @Autowired
    private lateinit var doctorRepository: DoctorRepository

    private lateinit var createUpdateDoctorDBService: CreateUpdateDoctorDBService

    @BeforeAll
    fun before() {
        createUpdateDoctorDBService = CreateUpdateDoctorDBService(doctorRepository)
    }

    @Test
    fun createNewDoctor() {
        var newDoctor = DoctorEntity("Hello", "World", dentistSpecialization)
        var createdDoctor = createUpdateDoctorDBService.createUpdateDoctor(newDoctor)

        assertEquals(newDoctor.firstName, createdDoctor.firstName)
        assertEquals(newDoctor.lastName, createdDoctor.lastName)
        assertEquals(newDoctor.specialization, createdDoctor.specialization)
        assertNotNull(createdDoctor.doctorId)
    }

    @Test
    fun updateExistingDoctor() {
        dentistDoctor.firstName = "Nowe_imie"
        createUpdateDoctorDBService.createUpdateDoctor(dentistDoctor)

        var updatedDoctor = doctorRepository.findById(dentistDoctor.doctorId!!)

        assertEquals(updatedDoctor.get().firstName, "Nowe_imie")
    }

}