package com.rsq.recruitment.persistence.db.services.patients

import com.rsq.recruitment.persistence.db.WithDatabaseTestData
import com.rsq.recruitment.persistence.db.repositories.PatientRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RetrievePatientDBServiceWithTestData : WithDatabaseTestData() {

    @Autowired
    private lateinit var patientRepository: PatientRepository

    private lateinit var retrievePatientDBService: RetrievePatientDBService

    @BeforeAll
    fun before() {
        retrievePatientDBService = RetrievePatientDBService(patientRepository)
    }

    @Test
    fun retrievePatientByIdSuccessfully() {
        var retrievedFirstPatient = retrievePatientDBService.retrievePatientById(firstPatient.patientId)
        assertTrue(retrievedFirstPatient.isPresent)
    }

    @Test
    fun retrievePatientByIdWhenDoesNotExists() {
        var retrievedPatient = retrievePatientDBService.retrievePatientById(-1L)
        assertTrue(retrievedPatient.isEmpty)
    }

    @Test
    fun retrievedAllPatients() {
        var allPatients = retrievePatientDBService.retrieveAllPatients()

        assertEquals(2, allPatients.size)
    }

}