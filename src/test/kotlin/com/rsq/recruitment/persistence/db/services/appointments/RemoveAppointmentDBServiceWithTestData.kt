package com.rsq.recruitment.persistence.db.services.appointments

import com.rsq.recruitment.persistence.db.WithDatabaseTestData
import com.rsq.recruitment.persistence.db.repositories.AppointmentRepository
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class RemoveAppointmentDBServiceWithTestData : WithDatabaseTestData() {

    @Autowired
    private lateinit var appointmentRepository: AppointmentRepository

    private lateinit var removeAppointmentDBService: RemoveAppointmentDBService

    @BeforeAll
    fun before() {
        removeAppointmentDBService = RemoveAppointmentDBService(appointmentRepository)
    }

    @Test
    fun removeAppointment() {
        removeAppointmentDBService.removeAppointment(appointmentForFirstPatientAndDentist.appointmentId)

        assertTrue(appointmentRepository.findById(appointmentForFirstPatientAndDentist.appointmentId!!).isEmpty)
    }
}