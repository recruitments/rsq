package com.rsq.recruitment.persistence.db.services.doctors

import com.rsq.recruitment.persistence.db.WithDatabaseTestData
import com.rsq.recruitment.persistence.db.repositories.DoctorRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RetrieveDoctorDBServiceWithTestData : WithDatabaseTestData() {

    @Autowired
    private lateinit var doctorRepository: DoctorRepository

    private lateinit var retrieveDoctorDBService: RetrieveDoctorDBService

    @BeforeAll
    fun before() {
        retrieveDoctorDBService = RetrieveDoctorDBService(doctorRepository)
    }

    @Test
    fun retrieveDoctorByIdSuccessfully() {
        var retrievedDentistDoctor = retrieveDoctorDBService.retrieveDoctorById(dentistDoctor.doctorId)
        assertTrue(retrievedDentistDoctor.isPresent)
    }

    @Test
    fun retrieveDoctorByIdWhenDoesNotExists() {
        var retrievedDentistDoctor = retrieveDoctorDBService.retrieveDoctorById(-1L)
        assertTrue(retrievedDentistDoctor.isEmpty)
    }

    @Test
    fun retrievedAllDoctors() {
        var allDoctors = retrieveDoctorDBService.retrieveAllDoctors()

        assertEquals(2, allDoctors.size)
    }

}