package com.rsq.recruitment.persistence.db.services.doctors

import com.rsq.recruitment.persistence.db.WithDatabaseTestData
import com.rsq.recruitment.persistence.db.repositories.AppointmentRepository
import com.rsq.recruitment.persistence.db.repositories.DoctorRepository
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RemoveDoctorDBServiceWithTestData : WithDatabaseTestData() {

    @Autowired
    private lateinit var doctorRepository: DoctorRepository

    @Autowired
    private lateinit var appointmentRepository: AppointmentRepository

    private lateinit var removeDoctorDBService: RemoveDoctorDBService

    @BeforeAll
    fun before() {
        removeDoctorDBService = RemoveDoctorDBService(doctorRepository)
    }

    @Test
    fun remove() {
        appointmentRepository.deleteAll()
        removeDoctorDBService.removeDoctor(dentistDoctor.doctorId)

        assertTrue(doctorRepository.findById(dentistDoctor.doctorId!!).isEmpty)
    }

}