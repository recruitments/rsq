package com.rsq.recruitment.persistence.db.services.appointments

import com.rsq.recruitment.persistence.db.WithDatabaseTestData
import com.rsq.recruitment.persistence.db.entities.AppointmentEntity
import com.rsq.recruitment.persistence.db.repositories.AppointmentRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import java.sql.Time
import java.util.*

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class MakeAnAppointmentDBServiceWithTestData : WithDatabaseTestData() {

    @Autowired
    private lateinit var appointmentRepository: AppointmentRepository

    private lateinit var makeAnAppointmentDBService: MakeAnAppointmentDBService

    @BeforeAll
    fun before() {
        makeAnAppointmentDBService = MakeAnAppointmentDBService(appointmentRepository)
    }

    @Test
    fun makeAnAppointmentAddingSuccess() {
        val date = Date(System.currentTimeMillis())
        val time = Time(System.currentTimeMillis())
        val address = "3 maja"
        val appointment = makeAnAppointmentDBService.saveAppointment(
                AppointmentEntity(
                        firstPatient,
                        dentistDoctor,
                        date,
                        time,
                        address
                )
        )

        assertEquals(firstPatient, appointment.patient)
        assertEquals(dentistDoctor, appointment.doctor)
        assertEquals(date, appointment.date)
        assertEquals(time, appointment.time)
        assertNotNull(appointment.appointmentId)
    }
}