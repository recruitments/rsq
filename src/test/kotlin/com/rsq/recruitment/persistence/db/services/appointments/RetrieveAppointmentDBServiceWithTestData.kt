package com.rsq.recruitment.persistence.db.services.appointments

import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.persistence.db.WithDatabaseTestData
import com.rsq.recruitment.persistence.db.repositories.AppointmentRepository
import com.rsq.recruitment.persistence.db.services.patients.RetrievePatientDBService
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class RetrieveAppointmentDBServiceWithTestData : WithDatabaseTestData() {

    private val NOT_EXISTING_PATIENT_ID: Long = -1

    private val NOT_EXISTING_APPOINTMENT_ID: Long = -1;

    @Autowired
    private lateinit var retrievePatientDBService: RetrievePatientDBService

    @Autowired
    private lateinit var appointmentRepository: AppointmentRepository

    private lateinit var retrieveAppointmentDBService: RetrieveAppointmentDBService

    @BeforeAll
    fun before() {
        retrieveAppointmentDBService = RetrieveAppointmentDBService(retrievePatientDBService, appointmentRepository)
    }

    @Test
    fun retrieveAll() {
        val allAppointments = retrieveAppointmentDBService.retrieveAll()

        Assertions.assertThat(allAppointments).hasSize(1)
    }

    @Test
    fun retrieveByPatientId() {
        val appointmentsForPatient = retrieveAppointmentDBService.retrieveByPatientId(firstPatient.patientId!!)

        Assertions.assertThat(appointmentsForPatient).hasSize(1)
    }

    @Test
    fun retrieveByPatientIdWhenPatientDoesNotExistsThrowResourceNotFound() {
        assertThrows(ResourceNotFound::class.java) {
            retrieveAppointmentDBService.retrieveByPatientId(NOT_EXISTING_PATIENT_ID)
        }
    }

    @Test
    fun hasPatientAnyAppointmentsTrue() {
        assertTrue(retrieveAppointmentDBService.hasPatientAnyAppointments(firstPatient.patientId!!))
    }

    @Test
    fun hasPatientAnyAppointmentsFalse() {
        assertFalse(retrieveAppointmentDBService.hasPatientAnyAppointments(secondPatient.patientId!!))
    }

    @Test
    fun hasDoctorAnyAppointmentsTrue() {
        assertTrue(retrieveAppointmentDBService.hasDoctorAnyAppointments(dentistDoctor.doctorId!!))
    }

    @Test
    fun hasDoctorAnyAppointmentsFalse() {
        assertFalse(retrieveAppointmentDBService.hasDoctorAnyAppointments(physiotherapistDoctor.doctorId!!))
    }

    @Test
    fun retrieveByAppointmentIdSuccessfully() {
        assertTrue(retrieveAppointmentDBService.retrieveByAppointmentId(appointmentForFirstPatientAndDentist.appointmentId!!).isPresent)
    }

    @Test
    fun retrieveByAppointmentIdWhenDoesNotExists() {
        assertTrue(retrieveAppointmentDBService.retrieveByAppointmentId(NOT_EXISTING_APPOINTMENT_ID).isEmpty)
    }

}