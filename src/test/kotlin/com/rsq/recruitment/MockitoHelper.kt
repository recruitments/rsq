package com.rsq.recruitment

import org.mockito.Mockito

const val DATE_OF_26_03_2020: Long = 1585260159123
const val TIME_OF_14_00: Long = 50400000

fun <T> any(): T {
    return Mockito.any<T>()
}