package com.rsq.recruitment.persistence.db.entities

import javax.persistence.*

@Table(name = "specializations")
@Entity
class SpecializationEntity(
        var name: String,
        @Id
        @Column(name = "specialization_id")
        @SequenceGenerator(name = "specializations_generator", sequenceName = "specializations_id_seq", allocationSize = 1, initialValue = 1)
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "specializations_generator")
        var specializationId: Int? = null
)