package com.rsq.recruitment.persistence.db.entities

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "translations")
class TranslationEntity(
        var pl: String,
        var en: String,
        @Id
        var translationName: String
) {

    fun translate(lang: String): String {
        return when (lang) {
            "pl" -> pl
            else -> defaultTranslation()
        }
    }

    private fun defaultTranslation(): String {
        return en;
    }

}