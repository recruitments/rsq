package com.rsq.recruitment.persistence.db.entities

import java.sql.Time
import java.util.*
import javax.persistence.*

@Table(name = "appointments")
@Entity
class AppointmentEntity(

        @ManyToOne
        @JoinColumn(name = "patient_id")
        var patient: PatientEntity,

        @ManyToOne
        @JoinColumn(name = "doctor_id")
        var doctor: DoctorEntity,

        var date: Date,

        var time: Time,

        var address: String,

        @Id
        @Column(name = "appointment_id")
        @SequenceGenerator(allocationSize = 1, name = "appointment_generator", sequenceName = "appointment_id_seq")
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "appointment_generator")
        var appointmentId: Long? = null
)