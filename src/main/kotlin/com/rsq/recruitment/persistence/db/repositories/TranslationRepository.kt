package com.rsq.recruitment.persistence.db.repositories

import com.rsq.recruitment.persistence.db.entities.TranslationEntity
import org.springframework.data.jpa.repository.JpaRepository

interface TranslationRepository : JpaRepository<TranslationEntity, String>