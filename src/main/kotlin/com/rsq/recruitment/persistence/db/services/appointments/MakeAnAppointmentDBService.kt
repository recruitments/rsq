package com.rsq.recruitment.persistence.db.services.appointments

import com.rsq.recruitment.persistence.db.entities.AppointmentEntity
import com.rsq.recruitment.persistence.db.repositories.AppointmentRepository
import org.springframework.stereotype.Service

@Service
class MakeAnAppointmentDBService(
        private val appointmentRepository: AppointmentRepository
) {

    fun saveAppointment(appointmentEntity: AppointmentEntity): AppointmentEntity {
        return appointmentRepository.save(appointmentEntity)
    }

}