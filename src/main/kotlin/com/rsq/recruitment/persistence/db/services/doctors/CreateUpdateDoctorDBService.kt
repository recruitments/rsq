package com.rsq.recruitment.persistence.db.services.doctors

import com.rsq.recruitment.persistence.db.entities.DoctorEntity
import com.rsq.recruitment.persistence.db.repositories.DoctorRepository
import org.springframework.stereotype.Service

@Service
class CreateUpdateDoctorDBService(
        private val doctorRepository: DoctorRepository
) {

    fun createUpdateDoctor(doctorEntity: DoctorEntity): DoctorEntity {
        return doctorRepository.save(doctorEntity)
    }

}