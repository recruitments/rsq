package com.rsq.recruitment.persistence.db.services.appointments

import com.rsq.recruitment.persistence.db.repositories.AppointmentRepository
import org.springframework.stereotype.Service

@Service
class RemoveAppointmentDBService(private val appointmentRepository: AppointmentRepository) {

    fun removeAppointment(appointmentId: Long?) {
        appointmentRepository.deleteById(appointmentId!!)
    }

}