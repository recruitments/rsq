package com.rsq.recruitment.persistence.db.services.appointments

import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.persistence.db.entities.AppointmentEntity
import com.rsq.recruitment.persistence.db.repositories.AppointmentRepository
import com.rsq.recruitment.persistence.db.services.patients.RetrievePatientDBService
import org.springframework.stereotype.Service
import java.util.*

@Service
class RetrieveAppointmentDBService(
        private val retrievePatientDBService: RetrievePatientDBService,
        private val appointmentRepository: AppointmentRepository
) {

    fun retrieveAll(): List<AppointmentEntity> {
        return appointmentRepository.findAll()
    }

    fun retrieveByPatientId(patientId: Long): List<AppointmentEntity> {
        if (retrievePatientDBService.retrievePatientById(patientId).isEmpty) {
            throw ResourceNotFound("Patient with given $patientId id does not exists")
        }
        return appointmentRepository.findAllByPatientPatientId(patientId)
    }

    fun retrieveByAppointmentId(appointmentId: Long): Optional<AppointmentEntity> {
        return appointmentRepository.findById(appointmentId)
    }

    fun hasPatientAnyAppointments(patientId: Long): Boolean {
        return appointmentRepository.hasPatientAnyAppointments(patientId)
    }

    fun hasDoctorAnyAppointments(doctorId: Long): Boolean {
        return appointmentRepository.hasDoctorAnyAppointments(doctorId)
    }

}