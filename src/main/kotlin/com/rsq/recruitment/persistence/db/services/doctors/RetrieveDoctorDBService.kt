package com.rsq.recruitment.persistence.db.services.doctors

import com.rsq.recruitment.persistence.db.entities.DoctorEntity
import com.rsq.recruitment.persistence.db.repositories.DoctorRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class RetrieveDoctorDBService(
        private val doctorRepository: DoctorRepository
) {

    fun retrieveDoctorById(doctorId: Long?): Optional<DoctorEntity> {
        return doctorRepository.findById(doctorId!!)
    }

    fun retrieveAllDoctors(): List<DoctorEntity> {
        return doctorRepository.findAll()
    }

}
