package com.rsq.recruitment.persistence.db.repositories

import com.rsq.recruitment.persistence.db.entities.SpecializationEntity
import org.springframework.data.jpa.repository.JpaRepository

interface SpecializationRepository : JpaRepository<SpecializationEntity, Int>