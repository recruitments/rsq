package com.rsq.recruitment.persistence.db.entities

import javax.persistence.*

@Table(name = "patients")
@Entity
class PatientEntity(
        @Column(name = "first_name")
        var firstName: String,

        @Column(name = "last_name")
        var lastName: String,

        var address: String? = null,

        @Id
        @Column(name = "patient_id")
        @SequenceGenerator(allocationSize = 1, name = "patients_generator", sequenceName = "patients_id_seq")
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "patients_generator")
        var patientId: Long? = null
)
