package com.rsq.recruitment.persistence.db.services.patients

import com.rsq.recruitment.persistence.db.entities.PatientEntity
import com.rsq.recruitment.persistence.db.repositories.PatientRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class RetrievePatientDBService(
        private val patientRepository: PatientRepository) {

    fun retrievePatientById(patientId: Long?): Optional<PatientEntity> {
        return patientRepository.findById(patientId!!)
    }

    fun retrieveAllPatients(): List<PatientEntity> {
        return patientRepository.findAll()
    }

}
