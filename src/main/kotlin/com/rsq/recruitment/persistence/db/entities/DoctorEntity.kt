package com.rsq.recruitment.persistence.db.entities

import javax.persistence.*

@Entity
@Table(name = "doctors")
class DoctorEntity(

        @Column(name = "first_name")
        var firstName: String,

        @Column(name = "last_name")
        var lastName: String,

        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "specializationId", referencedColumnName = "specialization_id")
        var specialization: SpecializationEntity,

        @Id
        @Column(name = "doctor_id")
        @SequenceGenerator(allocationSize = 1, name = "doctors_generator", sequenceName = "doctors_id_seq")
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "doctors_generator")
        var doctorId: Long? = null
)