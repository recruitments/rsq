package com.rsq.recruitment.persistence.db.repositories

import com.rsq.recruitment.persistence.db.entities.AppointmentEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface AppointmentRepository : JpaRepository<AppointmentEntity, Long> {

    fun findAllByPatientPatientId(patientId: Long): List<AppointmentEntity>

    @Query("SELECT count(*) > 0 FROM AppointmentEntity a WHERE a.patient.patientId = :patientId")
    fun hasPatientAnyAppointments(@Param("patientId") patientId: Long): Boolean

    @Query("SELECT count(*) > 0 FROM AppointmentEntity a WHERE a.doctor.doctorId = :doctorId")
    fun hasDoctorAnyAppointments(@Param("doctorId") doctorId: Long): Boolean

}