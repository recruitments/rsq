package com.rsq.recruitment.persistence.db.repositories

import com.rsq.recruitment.persistence.db.entities.PatientEntity
import org.springframework.data.jpa.repository.JpaRepository

interface PatientRepository : JpaRepository<PatientEntity, Long>