package com.rsq.recruitment.persistence.db.services.patients

import com.rsq.recruitment.persistence.db.repositories.PatientRepository
import org.springframework.stereotype.Service

@Service
class RemovePatientDBService(private val patientRepository: PatientRepository) {

    fun removePatient(patientId: Long?) {
        patientRepository.deleteById(patientId!!)
    }

}