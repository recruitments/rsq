package com.rsq.recruitment.persistence.db.services.patients

import com.rsq.recruitment.persistence.db.entities.PatientEntity
import com.rsq.recruitment.persistence.db.repositories.PatientRepository
import org.springframework.stereotype.Service

@Service
class CreateUpdatePatientDBService(
        private val patientRepository: PatientRepository
) {

    fun createUpdatePatient(patientEntity: PatientEntity): PatientEntity {
        return patientRepository.save(patientEntity)
    }

}