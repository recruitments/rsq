package com.rsq.recruitment.persistence.db.repositories

import com.rsq.recruitment.persistence.db.entities.DoctorEntity
import org.springframework.data.jpa.repository.JpaRepository

interface DoctorRepository : JpaRepository<DoctorEntity, Long>