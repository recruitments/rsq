package com.rsq.recruitment.persistence.db.services.doctors

import com.rsq.recruitment.persistence.db.repositories.DoctorRepository
import org.springframework.stereotype.Service

@Service
class RemoveDoctorDBService(private val doctorRepository: DoctorRepository) {

    fun removeDoctor(doctorId: Long?) {
        doctorRepository.deleteById(doctorId!!)
    }

}