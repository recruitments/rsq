package com.rsq.recruitment.businesslogic.appointments

import com.rsq.recruitment.businesslogic.appointments.beans.Appointment
import com.rsq.recruitment.mappers.AppointmentEntityToAppointment
import com.rsq.recruitment.persistence.db.services.appointments.RetrieveAppointmentDBService
import org.springframework.stereotype.Service

@Service
class RetrieveAppointmentBusinessLogic(
        private val appointmentDBService: RetrieveAppointmentDBService,
        private val appointmentEntityToAppointment: AppointmentEntityToAppointment
) {

    fun retrieveAllAppointments(): List<Appointment> {
        return appointmentDBService
                .retrieveAll()
                .map { appointmentEntityToAppointment.convert(it) }
    }

    fun retrieveAppointmentsForPatient(patientId: Long): List<Appointment> {
        return appointmentDBService
                .retrieveByPatientId(patientId)
                .map { appointmentEntityToAppointment.convert(it) }
    }

}