package com.rsq.recruitment.businesslogic.appointments.beans

import com.rsq.recruitment.businesslogic.doctors.beans.Doctor
import com.rsq.recruitment.businesslogic.patients.beans.Patient

class Appointment(
        var appointmentId: Long,
        var date: String,
        var hour: String,
        var address: String,
        var doctor: Doctor,
        var Patient: Patient
)