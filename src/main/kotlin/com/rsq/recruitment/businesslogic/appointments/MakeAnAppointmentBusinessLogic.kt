package com.rsq.recruitment.businesslogic.appointments

import com.rsq.recruitment.businesslogic.appointments.beans.Appointment
import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.communication.rest.requests.ChangeAppointmentDateRequest
import com.rsq.recruitment.communication.rest.requests.MakeAnAppointmentRequest
import com.rsq.recruitment.mappers.AppointmentEntityToAppointment
import com.rsq.recruitment.persistence.db.entities.AppointmentEntity
import com.rsq.recruitment.persistence.db.entities.DoctorEntity
import com.rsq.recruitment.persistence.db.entities.PatientEntity
import com.rsq.recruitment.persistence.db.services.appointments.MakeAnAppointmentDBService
import com.rsq.recruitment.persistence.db.services.appointments.RetrieveAppointmentDBService
import com.rsq.recruitment.persistence.db.services.doctors.RetrieveDoctorDBService
import com.rsq.recruitment.persistence.db.services.patients.RetrievePatientDBService
import com.rsq.recruitment.utils.toDate
import com.rsq.recruitment.utils.toTime
import org.springframework.stereotype.Service
import java.util.*

@Service
class MakeAnAppointmentBusinessLogic(
        private val doctorDBService: RetrieveDoctorDBService,
        private val patientDBService: RetrievePatientDBService,
        private val makeAnAppointmentDBService: MakeAnAppointmentDBService,
        private val retrieveAppointmentDBService: RetrieveAppointmentDBService,
        private val appointmentEntityToAppointment: AppointmentEntityToAppointment
) {

    fun makeAnAppointment(
            doctorId: Long,
            patientId: Long,
            appointment: MakeAnAppointmentRequest): Appointment {
        val doctor = doctorDBService.retrieveDoctorById(doctorId)
        val patient = patientDBService.retrievePatientById(patientId)
        validateThatMakeAnAppointmentIsPossible(doctor, patient, doctorId, patientId)
        val appointment = makeAnAppointmentDBService.saveAppointment(
                AppointmentEntity(
                        patient.get(),
                        doctor.get(),
                        toDate(appointment.date),
                        toTime(appointment.time),
                        appointment.address
                )
        )
        return appointmentEntityToAppointment.convert(appointment)
    }

    private fun validateThatMakeAnAppointmentIsPossible(
            doctor: Optional<DoctorEntity>,
            patient: Optional<PatientEntity>,
            doctorId: Long,
            patientId: Long) {
        if (doctor.isEmpty) {
            throw ResourceNotFound("Doctor with $doctorId does not exists. Making an appointment is impossible!")
        }
        if (patient.isEmpty) {
            throw ResourceNotFound("Patient with $patientId does not exists. Making an appointment is impossible!")
        }
    }

    fun changeAppointmentDate(
            appointmentId: Long,
            changeAppointmentDateRequest: ChangeAppointmentDateRequest): Appointment {
        val appointment = retrieveAppointmentDBService.retrieveByAppointmentId(appointmentId)
        validateThatChangeAppointmentDateIsPossible(appointment, appointmentId);
        val appointmentEntity = appointment.get()
        appointmentEntity.date = toDate(changeAppointmentDateRequest.date)
        appointmentEntity.time = toTime(changeAppointmentDateRequest.time)
        return appointmentEntityToAppointment.convert(
                makeAnAppointmentDBService.saveAppointment(appointmentEntity)
        )
    }

    private fun validateThatChangeAppointmentDateIsPossible(
            appointment: Optional<AppointmentEntity>,
            appointmentId: Long) {
        if (appointment.isEmpty) {
            throw ResourceNotFound("Appointment with given $appointmentId id does not exists. Changing date is impossible!")
        }
    }

}