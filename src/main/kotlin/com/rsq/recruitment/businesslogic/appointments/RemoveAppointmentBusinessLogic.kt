package com.rsq.recruitment.businesslogic.appointments

import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.persistence.db.services.appointments.RemoveAppointmentDBService
import com.rsq.recruitment.persistence.db.services.appointments.RetrieveAppointmentDBService
import org.springframework.stereotype.Service

@Service
class RemoveAppointmentBusinessLogic(
        private val retrieveAppointmentDBService: RetrieveAppointmentDBService,
        private val removeAppointmentDBService: RemoveAppointmentDBService
) {

    fun removeAppointmentById(appointmentId: Long) {
        if (retrieveAppointmentDBService.retrieveByAppointmentId(appointmentId).isEmpty) {
            throw ResourceNotFound("Appointment with given $appointmentId id already not exists!")
        }
        removeAppointmentDBService.removeAppointment(appointmentId)
    }

}