package com.rsq.recruitment.businesslogic.doctors

import com.rsq.recruitment.businesslogic.doctors.beans.Doctor
import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.mappers.DoctorEntityToDoctor
import com.rsq.recruitment.persistence.db.services.doctors.RetrieveDoctorDBService
import org.springframework.stereotype.Service

@Service
class RetrieveDoctorBusinessLogic(
        private val retrieveDoctorDBService: RetrieveDoctorDBService,
        private val doctorEntityToDoctor: DoctorEntityToDoctor
) {

    fun retrieveDoctorById(doctorId: Long): Doctor {
        val doctor = retrieveDoctorDBService
                .retrieveDoctorById(doctorId);
        if (doctor.isEmpty) {
            throw ResourceNotFound("Doctor with given $doctorId id does not exists!")
        }
        return doctorEntityToDoctor.convert(doctor.get())
    }

    fun retrieveAllDoctors(): List<Doctor> {
        return retrieveDoctorDBService
                .retrieveAllDoctors()
                .map { doctorEntityToDoctor.convert(it) }
    }

}