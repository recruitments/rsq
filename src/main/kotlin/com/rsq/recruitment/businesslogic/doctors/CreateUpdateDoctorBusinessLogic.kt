package com.rsq.recruitment.businesslogic.doctors

import com.rsq.recruitment.businesslogic.doctors.beans.Doctor
import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.communication.rest.requests.CreateDoctorRequest
import com.rsq.recruitment.communication.rest.requests.UpdateDoctorRequest
import com.rsq.recruitment.mappers.DoctorEntityToDoctor
import com.rsq.recruitment.persistence.db.entities.DoctorEntity
import com.rsq.recruitment.persistence.db.entities.SpecializationEntity
import com.rsq.recruitment.persistence.db.repositories.SpecializationRepository
import com.rsq.recruitment.persistence.db.services.doctors.CreateUpdateDoctorDBService
import com.rsq.recruitment.persistence.db.services.doctors.RetrieveDoctorDBService
import com.rsq.recruitment.utils.merge
import org.springframework.stereotype.Service
import java.util.*

@Service
class CreateUpdateDoctorBusinessLogic(
        private val retrieveDoctorDBService: RetrieveDoctorDBService,
        private val createUpdateDoctorDBService: CreateUpdateDoctorDBService,
        private val specializationRepository: SpecializationRepository,
        private val doctorEntityToDoctor: DoctorEntityToDoctor) {

    fun addDoctor(createDoctorRequest: CreateDoctorRequest): Doctor {
        val specialization = retrieveSpecialization(createDoctorRequest.specializationId!!);
        val doctorEntity = DoctorEntity(
                createDoctorRequest.firstName!!,
                createDoctorRequest.lastName!!,
                specialization)
        return doctorEntityToDoctor.convert(
                createUpdateDoctorDBService.createUpdateDoctor(doctorEntity))
    }

    fun updateDoctor(
            existingDoctorId: Long,
            updateDoctorRequest: UpdateDoctorRequest): Doctor {
        val existingDoctor = retrieveDoctor(existingDoctorId)
        existingDoctor.firstName = merge(updateDoctorRequest.firstName, existingDoctor.firstName)!!
        existingDoctor.lastName = merge(updateDoctorRequest.lastName, existingDoctor.lastName)!!
        val specializationId = merge(updateDoctorRequest.specializationId, existingDoctor.specialization.specializationId!!)
        existingDoctor.specialization = retrieveSpecialization(specializationId!!)
        return doctorEntityToDoctor.convert(
                createUpdateDoctorDBService.createUpdateDoctor(existingDoctor))
    }

    private fun retrieveDoctor(existingDoctorId: Long): DoctorEntity {
        val doctor = retrieveDoctorDBService.retrieveDoctorById(existingDoctorId)
        if (doctor.isEmpty) {
            throw ResourceNotFound("Doctor with given $existingDoctorId id does not exists!")
        }
        return doctor.get()
    }

    private fun retrieveSpecialization(specializationId: Int): SpecializationEntity {
        val specialization = specializationRepository.findById(specializationId)
        validateThatSpecializationExists(specialization, specializationId)
        return specialization.get()
    }

    private fun validateThatSpecializationExists(
            specializationEntity: Optional<SpecializationEntity>,
            specializationId: Int) {
        if (specializationEntity.isEmpty) {
            throw ResourceNotFound("Specialization with given $specializationId id does not exists so cannot perform request!")
        }
    }

}