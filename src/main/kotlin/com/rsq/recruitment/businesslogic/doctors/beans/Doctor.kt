package com.rsq.recruitment.businesslogic.doctors.beans

class Doctor(
        var doctorId: Long,
        var firstName: String,
        var lastName: String,
        var specialization: String
) {

}