package com.rsq.recruitment.businesslogic.doctors

import com.rsq.recruitment.businesslogic.exceptions.DoctorHasExistingAppointments
import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.persistence.db.services.appointments.RetrieveAppointmentDBService
import com.rsq.recruitment.persistence.db.services.doctors.RemoveDoctorDBService
import com.rsq.recruitment.persistence.db.services.doctors.RetrieveDoctorDBService
import org.springframework.stereotype.Service

@Service
class RemoveDoctorBusinessLogic(
        private val retrieveDoctorDBService: RetrieveDoctorDBService,
        private val retrieveAppointmentDBService: RetrieveAppointmentDBService,
        private val removeDoctorDBService: RemoveDoctorDBService
) {

    fun removeDoctorById(doctorId: Long) {
        validateThatRemovingDoctorIsPossible(doctorId);
        removeDoctorDBService.removeDoctor(doctorId)
    }

    private fun validateThatRemovingDoctorIsPossible(doctorId: Long) {
        if (retrieveDoctorDBService.retrieveDoctorById(doctorId).isEmpty) {
            throw ResourceNotFound("Doctor with given $doctorId id couldn't be removed because already not exists")
        }
        if (retrieveAppointmentDBService.hasDoctorAnyAppointments(doctorId)) {
            throw DoctorHasExistingAppointments("Doctor with given $doctorId id couldn't be removed because has appointments!")
        }
    }

}