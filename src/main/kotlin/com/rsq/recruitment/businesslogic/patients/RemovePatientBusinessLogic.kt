package com.rsq.recruitment.businesslogic.patients

import com.rsq.recruitment.businesslogic.exceptions.PatientHasExistingAppointments
import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.persistence.db.services.appointments.RetrieveAppointmentDBService
import com.rsq.recruitment.persistence.db.services.patients.RemovePatientDBService
import com.rsq.recruitment.persistence.db.services.patients.RetrievePatientDBService
import org.springframework.stereotype.Service

@Service
class RemovePatientBusinessLogic(
        private val retrievePatientDBService: RetrievePatientDBService,
        private val retrieveAppointmentDBService: RetrieveAppointmentDBService,
        private val removePatientDBService: RemovePatientDBService
) {

    fun removePatientById(patientId: Long) {
        validateThatRemovePatientIsPossible(patientId)
        removePatientDBService.removePatient(patientId)
    }

    private fun validateThatRemovePatientIsPossible(patientId: Long) {
        if (retrievePatientDBService.retrievePatientById(patientId).isEmpty) {
            throw ResourceNotFound("Patient with given $patientId id couldn't be removed because already not exists")
        }
        if (retrieveAppointmentDBService.hasPatientAnyAppointments(patientId)) {
            throw PatientHasExistingAppointments("Patient with given $patientId id couldn't be removed because has appointments")
        }
    }

}