package com.rsq.recruitment.businesslogic.patients

import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.businesslogic.patients.beans.Patient
import com.rsq.recruitment.communication.rest.requests.CreatePatientRequest
import com.rsq.recruitment.communication.rest.requests.UpdatePatientRequest
import com.rsq.recruitment.mappers.PatientEntityToPatient
import com.rsq.recruitment.persistence.db.entities.PatientEntity
import com.rsq.recruitment.persistence.db.services.patients.CreateUpdatePatientDBService
import com.rsq.recruitment.persistence.db.services.patients.RetrievePatientDBService
import com.rsq.recruitment.utils.merge
import org.springframework.stereotype.Service

@Service
class CreateUpdatePatientBusinessLogic(
        private val retrievePatientDBService: RetrievePatientDBService,
        private val createUpdatePatientDBService: CreateUpdatePatientDBService,
        private val patientEntityToPatient: PatientEntityToPatient
) {

    fun addPatient(createPatientRequest: CreatePatientRequest): Patient {
        val patientEntity = PatientEntity(
                createPatientRequest.firstName!!,
                createPatientRequest.lastName!!,
                createPatientRequest.address)
        return patientEntityToPatient.convert(
                createUpdatePatientDBService.createUpdatePatient(patientEntity))
    }

    fun updatePatient(
            existingPatientId: Long,
            updatePatientRequest: UpdatePatientRequest): Patient {
        val existingPatient = retrievePatient(existingPatientId)
        existingPatient.firstName = merge(updatePatientRequest.firstName, existingPatient.firstName)!!
        existingPatient.lastName = merge(updatePatientRequest.lastName, existingPatient.lastName)!!
        existingPatient.address = merge(updatePatientRequest.address, existingPatient.address)
        return patientEntityToPatient.convert(
                createUpdatePatientDBService.createUpdatePatient(existingPatient))
    }

    private fun retrievePatient(existingPatientId: Long): PatientEntity {
        val patient = retrievePatientDBService.retrievePatientById(existingPatientId)
        if (patient.isEmpty) {
            throw ResourceNotFound("Patient with given $existingPatientId id does not exists!")
        }
        return patient.get()
    }

}