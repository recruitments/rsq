package com.rsq.recruitment.businesslogic.patients

import com.rsq.recruitment.businesslogic.exceptions.ResourceNotFound
import com.rsq.recruitment.businesslogic.patients.beans.Patient
import com.rsq.recruitment.mappers.PatientEntityToPatient
import com.rsq.recruitment.persistence.db.services.patients.RetrievePatientDBService
import org.springframework.stereotype.Service

@Service
class RetrievePatientBusinessLogic(
        private val retrievePatientDBService: RetrievePatientDBService,
        private val patientEntityToPatient: PatientEntityToPatient
) {

    fun retrievePatientById(patientId: Long): Patient {
        val patient = retrievePatientDBService
                .retrievePatientById(patientId);
        if (patient.isEmpty) {
            throw ResourceNotFound("Patient with given $patientId id does not exists!")
        }
        return patientEntityToPatient.convert(patient.get())
    }

    fun retrieveAllPatients(): List<Patient> {
        return retrievePatientDBService
                .retrieveAllPatients()
                .map { patientEntityToPatient.convert(it) }
    }

}