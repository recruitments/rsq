package com.rsq.recruitment.businesslogic.patients.beans

class Patient(
        var patientId: Long,
        var firstName: String,
        var lastName: String,
        var address: String? = null
) {
}