package com.rsq.recruitment.businesslogic.exceptions

import com.rsq.recruitment.communication.rest.exceptions.ProcessingRequestException

class DoctorHasExistingAppointments : ProcessingRequestException {

    constructor(message: String)
            : super("DOCTOR_HAS_EXISTING_APPOINTMENT", message, 400, System.currentTimeMillis())

}