package com.rsq.recruitment.businesslogic.exceptions

import com.rsq.recruitment.communication.rest.exceptions.ProcessingRequestException

class PatientHasExistingAppointments : ProcessingRequestException {

    constructor(message: String)
            : super("PATIENT_HAS_EXISTING_APPOINTMENT", message, 400, System.currentTimeMillis())

}