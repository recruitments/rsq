package com.rsq.recruitment.businesslogic.exceptions

import com.rsq.recruitment.communication.rest.exceptions.ProcessingRequestException

class ResourceNotFound : ProcessingRequestException {

    constructor(message: String)
            : super("ENTITY_NOT_FOUND", message, 404, System.currentTimeMillis())

}