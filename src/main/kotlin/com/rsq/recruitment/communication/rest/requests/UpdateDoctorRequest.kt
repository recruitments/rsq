package com.rsq.recruitment.communication.rest.requests

import javax.validation.constraints.Min
import javax.validation.constraints.Pattern

class UpdateDoctorRequest(

        @field:Pattern(regexp = "[\\p{IsAlphabetic}]{3,50}", message = "First name should be filled and containing from 3 to 50 letters!")
        var firstName: String? = null,

        @field:Pattern(regexp = "[\\p{IsAlphabetic}]{3,50}", message = "First name should be filled and containing from 3 to 50 letters!")
        var lastName: String? = null,

        @field:Min(value = 1, message = "Specialization id should be minimum 1")
        var specializationId: Int? = null
)