package com.rsq.recruitment.communication.rest.requests

import javax.validation.constraints.Pattern

class UpdatePatientRequest(

        @field:Pattern(regexp = "[\\p{IsAlphabetic}]{3,50}", message = "First name should be filled and containing from 3 to 50 letters!")
        var firstName: String? = null,

        @field:Pattern(regexp = "[\\p{IsAlphabetic}]{3,50}", message = "Last name should be filled and containing from 3 to 50 letters!")
        var lastName: String? = null,

        @field:Pattern(regexp = "[\\p{IsAlphabetic}]{3,50}", message = "Address should be filled and containing from 3 to 50 letters!")
        var address: String? = null
)