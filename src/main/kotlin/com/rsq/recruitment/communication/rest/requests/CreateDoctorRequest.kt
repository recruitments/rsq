package com.rsq.recruitment.communication.rest.requests

import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

class CreateDoctorRequest(

        @field:NotNull(message = "First name must not be null")
        @field:Pattern(regexp = "[\\p{IsAlphabetic}]{3,50}", message = "First name should be filled and containing from 3 to 50 letters!")
        var firstName: String? = null,

        @field:NotNull(message = "Last name must not be null")
        @field:Pattern(regexp = "[\\p{IsAlphabetic}]{3,50}", message = "Last name should be filled and containing from 3 to 50 letters!")
        var lastName: String? = null,

        @field:NotNull(message = "Specialization should be specified!")
        @field:Min(value = 1, message = "Specialization id should be minimum 1")
        var specializationId: Int? = null
) {
}