package com.rsq.recruitment.communication.rest

import com.rsq.recruitment.businesslogic.appointments.RetrieveAppointmentBusinessLogic
import com.rsq.recruitment.businesslogic.appointments.beans.Appointment
import com.rsq.recruitment.businesslogic.patients.CreateUpdatePatientBusinessLogic
import com.rsq.recruitment.businesslogic.patients.RemovePatientBusinessLogic
import com.rsq.recruitment.businesslogic.patients.RetrievePatientBusinessLogic
import com.rsq.recruitment.businesslogic.patients.beans.Patient
import com.rsq.recruitment.communication.rest.requests.CreatePatientRequest
import com.rsq.recruitment.communication.rest.requests.UpdatePatientRequest
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/patients")
class PatientRestController(
        private val retrievePatientBusinessLogic: RetrievePatientBusinessLogic,
        private val removePatientBusinessLogic: RemovePatientBusinessLogic,
        private val retrieveAppointmentBusinessLogic: RetrieveAppointmentBusinessLogic,
        private val createUpdatePatientBusinessLogic: CreateUpdatePatientBusinessLogic) {

    @GetMapping("/{patientId}")
    fun getPatientById(@PathVariable patientId: Long): Patient? {
        return retrievePatientBusinessLogic.retrievePatientById(patientId)
    }

    @GetMapping
    fun getAllPatients(): List<Patient> {
        return retrievePatientBusinessLogic.retrieveAllPatients()
    }

    @DeleteMapping("/{patientId}")
    fun deletePatient(@PathVariable patientId: Long) {
        return removePatientBusinessLogic.removePatientById(patientId)
    }

    @GetMapping("/{patientId}/appointments")
    fun appointments(@PathVariable patientId: Long): List<Appointment> {
        return retrieveAppointmentBusinessLogic.retrieveAppointmentsForPatient(patientId)
    }

    @PostMapping
    fun addPatient(@Valid @RequestBody createPatientRequest: CreatePatientRequest): Patient {
        return createUpdatePatientBusinessLogic.addPatient(createPatientRequest)
    }

    @PutMapping("/{patientId}")
    fun updatePatient(
            @PathVariable patientId: Long,
            @Valid @RequestBody updatePatientRequest: UpdatePatientRequest): Patient {
        return createUpdatePatientBusinessLogic.updatePatient(patientId, updatePatientRequest)
    }

}