package com.rsq.recruitment.communication.rest

import com.rsq.recruitment.businesslogic.doctors.CreateUpdateDoctorBusinessLogic
import com.rsq.recruitment.businesslogic.doctors.RemoveDoctorBusinessLogic
import com.rsq.recruitment.businesslogic.doctors.RetrieveDoctorBusinessLogic
import com.rsq.recruitment.businesslogic.doctors.beans.Doctor
import com.rsq.recruitment.communication.rest.requests.CreateDoctorRequest
import com.rsq.recruitment.communication.rest.requests.UpdateDoctorRequest
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/doctors")
class DoctorRestController(
        private val retrieveDoctorBusinessLogic: RetrieveDoctorBusinessLogic,
        private val removeDoctorBusinessLogic: RemoveDoctorBusinessLogic,
        private val createUpdateDoctorBusinessLogic: CreateUpdateDoctorBusinessLogic) {

    @GetMapping("/{doctorId}")
    fun getDoctorById(@PathVariable doctorId: Long): Doctor {
        return retrieveDoctorBusinessLogic.retrieveDoctorById(doctorId)
    }

    @GetMapping
    fun getAllDoctors(): List<Doctor> {
        return retrieveDoctorBusinessLogic.retrieveAllDoctors()
    }

    @DeleteMapping("/{doctorId}")
    fun deleteDoctor(@PathVariable doctorId: Long) {
        return removeDoctorBusinessLogic.removeDoctorById(doctorId)
    }

    @PostMapping
    fun addDoctor(@Valid @RequestBody createDoctorRequest: CreateDoctorRequest): Doctor {
        return createUpdateDoctorBusinessLogic.addDoctor(createDoctorRequest)
    }

    @PutMapping("/{doctorId}")
    fun updateDoctor(
            @PathVariable doctorId: Long,
            @Valid @RequestBody updateDoctorRequest: UpdateDoctorRequest): Doctor {
        return createUpdateDoctorBusinessLogic.updateDoctor(doctorId, updateDoctorRequest)
    }

}