package com.rsq.recruitment.communication.rest.requests

import javax.validation.constraints.Pattern

class ChangeAppointmentDateRequest(

        @field:Pattern(regexp = "[2-9][0-9]{3}-((1[12])|(0[1-9]))-((0[1-9])|([1-2][0-9])|(3[01]))", message = "Date should be in yyyy-MM-dd format")
        var date: String,

        @field:Pattern(regexp = "(([01][0-9])|(2[1-3])):[0-5][0-9]", message = "Time should be in HH:mm format")
        var time: String

) {
}