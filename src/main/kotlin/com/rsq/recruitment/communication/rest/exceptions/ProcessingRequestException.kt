package com.rsq.recruitment.communication.rest.exceptions

abstract class ProcessingRequestException : RuntimeException {

    private var reason: String

    private var status: Int

    private var timestamp: Long

    constructor(reason: String, message: String, status: Int, timestamp: Long)
            : super(message) {
        this.reason = reason
        this.status = status
        this.timestamp = timestamp
    }

    fun getReason(): String {
        return reason
    }

    fun getStatus(): Int {
        return status
    }

    fun getTimestamp(): Long {
        return timestamp
    }

}