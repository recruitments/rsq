package com.rsq.recruitment.communication.rest

import com.rsq.recruitment.businesslogic.appointments.MakeAnAppointmentBusinessLogic
import com.rsq.recruitment.businesslogic.appointments.RemoveAppointmentBusinessLogic
import com.rsq.recruitment.businesslogic.appointments.RetrieveAppointmentBusinessLogic
import com.rsq.recruitment.businesslogic.appointments.beans.Appointment
import com.rsq.recruitment.communication.rest.requests.ChangeAppointmentDateRequest
import com.rsq.recruitment.communication.rest.requests.MakeAnAppointmentRequest
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

@RestController
class AppointmentRestController(
        private val retrieveAppointmentBusinessLogic: RetrieveAppointmentBusinessLogic,
        private val makeAnAppointmentBusinessLogic: MakeAnAppointmentBusinessLogic,
        private val removeAppointmentBusinessLogic: RemoveAppointmentBusinessLogic
) {

    @GetMapping("/appointments")
    fun retrieveAllAppointments(): List<Appointment> {
        return retrieveAppointmentBusinessLogic.retrieveAllAppointments()
    }

    @PutMapping("/appointments/{appointmentId}")
    fun changeAppointmentDate(
            @PathVariable appointmentId: Long,
            @Validated @RequestBody changeAppointmentDateRequest: ChangeAppointmentDateRequest): Appointment {
        return makeAnAppointmentBusinessLogic.changeAppointmentDate(appointmentId, changeAppointmentDateRequest)
    }

    @DeleteMapping("/appointments/{appointmentId}")
    fun removeAppointment(@PathVariable appointmentId: Long) {
        removeAppointmentBusinessLogic.removeAppointmentById(appointmentId)
    }

    @PostMapping("/doctors/{doctorId}/patients/{patientId}/appointments")
    fun makeAppointment(
            @PathVariable doctorId: Long,
            @PathVariable patientId: Long,
            @Validated @RequestBody appointmentRequest: MakeAnAppointmentRequest): Appointment {
        return makeAnAppointmentBusinessLogic.makeAnAppointment(doctorId, patientId, appointmentRequest)
    }

}