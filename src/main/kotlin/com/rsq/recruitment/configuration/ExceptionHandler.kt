package com.rsq.recruitment.configuration

import com.rsq.recruitment.communication.rest.exceptions.ProcessingRequestException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestController
import java.sql.Timestamp

@ControllerAdvice
@RestController
class ExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun onInvalidInputRequestExceptionHandler(exception: MethodArgumentNotValidException): ResponseEntity<InvalidInputErrorResponse> {
        var invalidFields: MutableList<InvalidField> = ArrayList()
        exception.bindingResult.fieldErrors.forEach {
            invalidFields
                    .add(
                            InvalidField(
                                    it.field,
                                    it.defaultMessage!!
                            )
                    )
        }
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(
                        InvalidInputErrorResponse(
                                "INVALID_INPUT",
                                "Provided input is invalid. Below are found errors, try to correct them!",
                                400,
                                Timestamp(System.currentTimeMillis()),
                                invalidFields
                        )
                )
    }

    @ExceptionHandler(ProcessingRequestException::class)
    fun onProcessingRequestExceptionHandler(
            exception: ProcessingRequestException): ResponseEntity<ErrorResponse> {
        return ResponseEntity
                .status(HttpStatus.valueOf(exception.getStatus()))
                .body(
                        ErrorResponse(
                                exception.getReason(),
                                exception.message!!,
                                exception.getStatus(),
                                Timestamp(exception.getTimestamp())
                        )
                )
    }

    @ExceptionHandler(Exception::class)
    fun onExceptionHandler(exception: Exception): ResponseEntity<ErrorResponse> {
        return ResponseEntity
                .status(500)
                .body(
                        ErrorResponse(
                                "INTERNAL_ERROR",
                                exception.message!!,
                                500,
                                Timestamp(System.currentTimeMillis())
                        )
                )
    }

}

class ErrorResponse(
        var reason: String,
        var message: String,
        var status: Int,
        var timestamp: Timestamp
)

class InvalidInputErrorResponse(
        var reason: String,
        var message: String,
        var status: Int,
        var timestamp: Timestamp,
        var invalidFields: List<InvalidField>
)

class InvalidField(
        var field: String,
        var message: String
)