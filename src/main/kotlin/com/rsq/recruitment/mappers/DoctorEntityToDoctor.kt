package com.rsq.recruitment.mappers

import com.rsq.recruitment.businesslogic.doctors.beans.Doctor
import com.rsq.recruitment.persistence.db.entities.DoctorEntity
import org.springframework.stereotype.Service

@Service
class DoctorEntityToDoctor(
        private val translationService: TranslationService
) {

    fun convert(doctorEntity: DoctorEntity): Doctor {
        return Doctor(
                doctorEntity.doctorId!!,
                doctorEntity.firstName,
                doctorEntity.lastName,
                translationService.translate(
                        doctorEntity.specialization.name,
                        doctorEntity.specialization.name.toLowerCase().capitalize()
                ))
    }

}