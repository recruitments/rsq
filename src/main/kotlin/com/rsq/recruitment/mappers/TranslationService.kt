package com.rsq.recruitment.mappers

import com.rsq.recruitment.persistence.db.entities.TranslationEntity
import com.rsq.recruitment.persistence.db.repositories.TranslationRepository
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.stereotype.Service

@Service
class TranslationService(
        private val translationRepository: TranslationRepository
) {

    fun translate(key: String, defaultValue: String): String {
        return translate(key, defaultValue, LocaleContextHolder.getLocale().language)
    }

    fun translate(key: String, defaultValue: String, lang: String): String {
        return translationRepository
                .findById(key)
                .orElse(TranslationEntity(defaultValue, defaultValue, key))
                .translate(lang)
    }

}