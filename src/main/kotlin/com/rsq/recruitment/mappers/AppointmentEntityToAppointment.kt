package com.rsq.recruitment.mappers

import com.rsq.recruitment.businesslogic.appointments.beans.Appointment
import com.rsq.recruitment.persistence.db.entities.AppointmentEntity
import com.rsq.recruitment.utils.fromDate
import com.rsq.recruitment.utils.fromTime
import org.springframework.stereotype.Service

@Service
class AppointmentEntityToAppointment(
        private val doctorEntityToDoctor: DoctorEntityToDoctor,
        private val patientEntityToPatient: PatientEntityToPatient
) {

    fun convert(appointmentEntity: AppointmentEntity): Appointment {
        val doctor = doctorEntityToDoctor.convert(appointmentEntity.doctor)
        val patient = patientEntityToPatient.convert(appointmentEntity.patient)
        return Appointment(
                appointmentEntity.appointmentId!!,
                fromDate(appointmentEntity.date),
                fromTime(appointmentEntity.time),
                appointmentEntity.address,
                doctor,
                patient)
    }

}