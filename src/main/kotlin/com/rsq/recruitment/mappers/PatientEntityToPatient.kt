package com.rsq.recruitment.mappers;

import com.rsq.recruitment.businesslogic.patients.beans.Patient
import com.rsq.recruitment.persistence.db.entities.PatientEntity
import org.springframework.stereotype.Service;

@Service
class PatientEntityToPatient {

    fun convert(patientEntity: PatientEntity): Patient {
        return Patient(patientEntity.patientId!!, patientEntity.firstName, patientEntity.lastName, patientEntity.address)
    }

}
