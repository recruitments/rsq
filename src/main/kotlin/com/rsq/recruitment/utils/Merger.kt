package com.rsq.recruitment.utils

fun <T> merge(mostSignificantValue: T?, fallbackValue: T?): T? {
    if (mostSignificantValue != null) return mostSignificantValue
    return fallbackValue
}