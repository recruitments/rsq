package com.rsq.recruitment.utils

import java.sql.Time

private val HOUR_TO_MINUTS: Long = 60

private val MINUTES_TO_MILLISECONDS: Long = 60000

fun fromTime(time: Time): String {
    val timeInMinutes = time.time / MINUTES_TO_MILLISECONDS
    val hours = timeInMinutes / HOUR_TO_MINUTS
    val minutes = timeInMinutes % HOUR_TO_MINUTS
    val hoursString = formatTime(hours)
    val minutesString = formatTime(minutes)
    return "$hoursString:$minutesString"
}

fun toTime(timeInStringFormat: String): Time {
    val hourMinutes = timeInStringFormat.split(":")
    if (hourMinutes.size != 2) {
        throw RuntimeException("Wrong time format!")
    }
    return toTime(hourMinutes[0], hourMinutes[1])
}

private fun formatTime(time: Long): String {
    if (time < 10) {
        return "0$time"
    }
    return "$time"
}

private fun toTime(hoursInStringFormat: String, minutesInStringFormat: String): Time {
    try {
        return toTime(Integer.valueOf(hoursInStringFormat).toLong(), Integer.valueOf(minutesInStringFormat).toLong())
    } catch (e: RuntimeException) {
        throw RuntimeException("Not a number")
    }
}

private fun toTime(hours: Long, minutes: Long): Time {
    return Time((hours * HOUR_TO_MINUTS + minutes) * MINUTES_TO_MILLISECONDS)
}