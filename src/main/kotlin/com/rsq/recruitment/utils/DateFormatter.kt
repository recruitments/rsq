package com.rsq.recruitment.utils

import java.text.SimpleDateFormat
import java.time.Instant
import java.time.ZoneOffset
import java.util.*

private val formatter = SimpleDateFormat("yyyy-MM-dd")

fun fromDate(date: Date): String {
    return formatter.format(date)
}

fun toDate(dateInStringFormat: String): Date {
    return Date(formatter.parse(dateInStringFormat).time + timezoneOffsetInMillis())
}

private fun timezoneOffsetInMillis(): Long {
    return ZoneOffset
            .systemDefault()
            .rules
            .getOffset(Instant.now())
            .totalSeconds.toLong() * 1000
}